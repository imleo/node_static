FROM node:11.9

WORKDIR /usr/src/app

COPY package.json .

# install dependencies
RUN npm install

COPY server.js .

CMD [ "npm", "start" ]
